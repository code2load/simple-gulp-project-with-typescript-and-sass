var gulp = require('gulp');
const browsersync = require("browser-sync");

var sass = require('gulp-sass');
var minifyCSS = require('gulp-csso');

var ts = require("gulp-typescript");
var tsProject = ts.createProject("tsconfig.json");


gulp.task('html', function(){
  return gulp.src('client/html/*.html')
    .on('error', function (err) {
        console.error('Error!', err.message);
    })
    .pipe(gulp.dest('build'));
});

gulp.task('css', function(){
  return gulp.src('client/scss/*.scss')
    .on('error', function (err) {
        console.error('Error!', err.message);
    })
    .pipe(sass())
    .pipe(minifyCSS())
    .pipe(gulp.dest('build/css'))
    .pipe(browsersync.stream());
});

gulp.task('typescript', function () {
    return tsProject.src()
        .on('error', function (err) {
            console.error('Error!', err.message);
        })  
        .pipe(tsProject())
        .js.pipe(gulp.dest("build/js"));
});

gulp.task('serve', ['html', 'css', 'typescript'], function(){
    browsersync.init({
        server: './build'
    })

    gulp.watch(['client/scss/*.scss'], ['css']).on('change', browsersync.reload)
    gulp.watch(['client/ts/*.ts'], ['typescript']).on('change', browsersync.reload)
    gulp.watch(['client/html/*.html'], ['html']).on('change', browsersync.reload)
});

gulp.task('default', ['html', 'css', 'typescript']);